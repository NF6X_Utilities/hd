/**************************************************************************
 * Copyright (C) 2009 Mark J. Blair, NF6X
 *
 * This file is part of hd.
 *
 *  hd is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hd is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hd.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

#include "hd.h"


int main(int argc, char **argv) {

    uint32_t	blocksize = 0;
    int		opt;
    FILE	*ip;
    FILE	*op;
    int		offsetflag;


    /* Print offsets by default */
    offsetflag = 1;

    /* Disable getopt()'s internal error messages */
    opterr = 0;

    /* Parse command-line arguments */
    while ((opt = getopt(argc, argv, ":b:nhv")) != -1) {

	switch (opt) {

	  case 'b':
	    blocksize = strtoul(optarg, NULL, 0);
	    break;

	  case 'n':
	    offsetflag = 0;
	    break;

	  case 'h':
	    /* print help and exit */
	    print_version(stdout);
	    printf("\n");
	    print_description(stdout);
	    printf("\n");
	    print_usage(stdout);
	    exit(0);
	    break;

	  case 'v':
	    /* print version and exit */
	    print_version(stdout);
	    exit(0);
	    break;

	  case ':':
	    /* Missing argument */
	    fprintf(stderr, "ERROR: Option \"-%c\" requires an argument.\n\n", optopt);
	    print_usage(stderr);
	    exit(1);
	    break;
	    
	  case '?':
	    /* Error! */
	    fprintf(stderr, "ERROR: Illegal option \"-%c\".\n\n", optopt);
	    print_usage(stderr);
	    exit(1);
	    break;

	  default:
	    /* Error! */
	    fprintf(stderr, "ERROR: Option parser error.\n\n");
	    print_usage(stderr);
	    exit(1);
	    break;
	}
    }
    
    argc -= optind;
    argv += optind;


    /* Now that we have parsed the options, there should be zero or one remaining
       arguments */
    switch (argc) {

      case 0:
	ip = stdin;
	op = stdout;
	break;

      case 1:
	ip = fopen(argv[0], "rb");
	if (!ip) {
	    perror("Error opening input file");
	    exit(1);
	}
	op = stdout;
	break;

      default:
	fprintf(stderr, "ERROR: Wrong number of arguments.\n\n");
	print_usage(stderr);
	exit(1);
	break;
    }


    hexdump(ip, op, blocksize, offsetflag);


    exit(0);
}

