/**************************************************************************
 * Copyright (C) 2009 Mark J. Blair, NF6X
 *
 * This file is part of hd.
 *
 *  hd is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hd is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hd.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

#include "hd.h"

#define ADDR_WIDTH 10

/* Calculate output buffer position of first character of hex value */
#define HEX_POS(a) (ADDR_WIDTH + ((a)*2) + (a) + ((a)/4))

/* Calculate output buffer positon of printed ASCII character */
#define ASCII_POS(a) (HEX_POS(BYTES_PER_LINE-1) + 5 + (a))

#define LINE_BUF_SIZE (ASCII_POS(BYTES_PER_LINE-1) + 4)

static char hexchars[16] = "0123456789ABCDEF";


void hexdump(FILE *ip, FILE *op, uint32_t blocksize, int offsetflag) {
    uint32_t	addr, block;
    uint8_t	inbuf[BYTES_PER_LINE];
    char	linebuf[LINE_BUF_SIZE];
    int		bytes;
    int		n;

    /* Blocksize must be a multiple of BYTES_PER_LINE, or 0 to disable blocking */
    if (blocksize && (blocksize % BYTES_PER_LINE)) {
	fprintf(stderr, "ERROR: Blocksize must be a multiple of %d.\n", BYTES_PER_LINE);
	exit(1);
    }

	
    /* Initialize line output buffer */
    for (n = 0; n < LINE_BUF_SIZE; linebuf[n++] = ' ');
    linebuf[ASCII_POS(0) - 1] = '|';
    linebuf[ASCII_POS(BYTES_PER_LINE-1) + 1] = '|';
    linebuf[ASCII_POS(BYTES_PER_LINE-1) + 2] = '\n';
    linebuf[ASCII_POS(BYTES_PER_LINE-1) + 3] = 0;

    addr = block = 0;

    while (!feof(ip)) {

	bytes = fread(inbuf, 1, BYTES_PER_LINE, ip);

	if (bytes > 0) {

	    /* Check for block boundary */
	    if (blocksize && ((addr % blocksize) == 0)) {
		fprintf(op, "\nBlock %8.8X\n", block);
		++block;
	    }

	    /* Print address of character at start of line */
	    if (offsetflag) {
		sprintf(linebuf, "%8.8X", addr);
		linebuf[8] = ' ';
	    } else {
		sprintf(linebuf, "        ");
		linebuf[8] = ' ';
	    }

	    /* Print hex and ASCII values of read bytes */

	    for (n = 0; n < bytes; n++) {
		linebuf[HEX_POS(n)]     = hexchars[inbuf[n] >> 4];
		linebuf[HEX_POS(n) + 1] = hexchars[inbuf[n] & 0x0f];
		linebuf[ASCII_POS(n)]   = isprint(inbuf[n]) ? inbuf[n] : '.';
	    }

	    /* If fewer than BYTES_PER_LINE bytes were read, then replace
	       remaining hex and ASCII values with spaces. */

	    for ( ; n < BYTES_PER_LINE; n++) {
		linebuf[HEX_POS(n)]     = ' ';
		linebuf[HEX_POS(n) + 1] = ' ';
		linebuf[ASCII_POS(n)]   = ' ';
	    }
	
	    /* Write line to output file */
	    fputs(linebuf, op);

	    addr += bytes;
	}

    }

}
