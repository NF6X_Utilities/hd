/**************************************************************************
 * Copyright (C) 2009 Mark J. Blair, NF6X
 *
 * This file is part of hd.
 *
 *  hd is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hd is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hd.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

#include "hd.h"


void print_version(FILE *fp) {
    fprintf(fp, "%s: Copyright 2009 by %s, released under GPLv3\n",
	    PACKAGE_STRING, PACKAGE_BUGREPORT);
}


void print_description(FILE *fp) {
    fprintf(fp, "Description:\n");
    fprintf(fp, "  hd is hexadecimal dump program which can optionally divide\n");
    fprintf(fp, "  output into blocks of a specified size. Each output line\n");
    fprintf(fp, "  consists of a byte offset, followed by %d hexadecimal values,\n",
	    BYTES_PER_LINE);
    fprintf(fp, "  followed by the ASCII characters corresponding to those values\n");
    fprintf(fp, "  (replaced by '.' if non-printable). The ASCII values are delimited\n");
    fprintf(fp, "  by '|' characters.\n");
}


void print_usage(FILE *fp) {
    fprintf(fp, "Usage:\n");
    fprintf(fp, "  hd [-n] [-b <blocksize>] [<infile>]\n");
    fprintf(fp, "  hd -h\n");
    fprintf(fp, "  hd -v\n");

    fprintf(fp, "\n");
    fprintf(fp, "Options:\n");
    fprintf(fp, "  -b: Divide output into blocks of <blocksize> bytes.\n");
    fprintf(fp, "      <blocksize> must be divisible by %d.\n", BYTES_PER_LINE);
    fprintf(fp, "  -n: No byte offsets printed.\n");
    fprintf(fp, "  -h: Print help to stdout and exit.\n");
    fprintf(fp, "  -v: Print version string to stdout and exit.\n");

    fprintf(fp, "\n");
    fprintf(fp, "Input will be read from stdin if <infile> is not specified.\n");
}


